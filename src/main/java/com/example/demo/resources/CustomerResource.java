package com.example.demo.resources;

import com.example.demo.domain.model.Customer;

public class CustomerResource {

    private Long id;
    private String first;
    private String last;
    private String address;
    private String gender;
    private String thumbnail;

    public CustomerResource(Customer customer){

        this.setId(customer.getId());
        this.setFirst(customer.getFirstName());
        this.setLast(customer.getLastName());
        this.setAddress(customer.getAddress());
        this.setGender(customer.getGender().getName());
        this.setThumbnail(customer.getImageUrl());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public String getLast() {
        return last;
    }

    public void setLast(String last) {
        this.last = last;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }
}
