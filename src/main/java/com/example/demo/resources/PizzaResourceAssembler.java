package com.example.demo.resources;

import com.example.demo.domain.model.Pizza;
import org.springframework.stereotype.Component;

@Component
public class PizzaResourceAssembler {
    @Override
    public PizzaResource toResource(Pizza domainObject) {
        return new PizzaResource(domainObject);
    }
}
