package com.example.demo.resources;

import org.springframework.hateoas.mvc.TypeReferences;

import java.util.Collection;
import java.util.stream.Collectors;

public abstract class ResourceAssembler {

    public abstract TypeReferences.ResourceType toResource(DomainType domainObject);

    public Collection<TypeReferences.ResourceType> toResourceCollection(Collection<DomainType> domainObjects){
        return domainObjects
                .stream()
                .map(o -> toResource(o))
                .collect(Collectors.toList());
    }
}
