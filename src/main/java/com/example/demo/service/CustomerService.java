package com.example.demo.service;

import com.example.demo.domain.model.Customer;
import com.example.demo.domain.model.Order;
import com.example.demo.domain.repositoy.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    public Iterable<Customer> getAllCustomers(){
        return customerRepository.findAll();
    }

    public Optional<Customer> getCustomerById(Long id) {
        return customerRepository.findById(id);
    }

    public Customer createCustomer(Customer customer){
        return customerRepository.save(customer);
    }

    public Boolean deleteCustomer(Long id){

        Boolean wasDeleted = Boolean.FALSE;
        Optional<Customer> customer = getCustomerById(id);

        if (customer.isPresent()){
            customerRepository.delete(customer.get());
            wasDeleted = Boolean.TRUE;
        }

        return wasDeleted;

    }

    public Boolean updateCustomer(Long id, Customer updatedCustomer){

        Boolean wasUpdated = Boolean.FALSE;

        Optional<Customer> customer = getCustomerById(id);
        if(customer.isPresent()){
            customer.get().setFirstName(updatedCustomer.getFirstName());
            customer.get().setLastName(updatedCustomer.getLastName());
            customerRepository.save(customer.get());
            wasUpdated = Boolean.TRUE;
        }

        return wasUpdated;
    }


    public Set<Order> getOrdersById(Long customerId){

        Set<Order> orders = new HashSet<>();
        Optional<Customer> customer = customerRepository.findById(customerId);

        if(customer.isPresent()){
            orders = customer.get().getOrders();
        }

        return orders;
    }
}
