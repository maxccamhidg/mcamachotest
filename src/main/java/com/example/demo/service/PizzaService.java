package com.example.demo.service;

import com.example.demo.domain.model.Pizza;
import com.example.demo.domain.repositoy.PizzaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PizzaService {

    @Autowired
    private PizzaRepository pizzaRepository;


    public List<Pizza> getAllPizzas(){

        List<Pizza> pizzas = new ArrayList<>();
        pizzaRepository.findAll().forEach( p -> pizzas.add(p));

        return pizzas;
    }

    public Optional<Pizza> getPizzaById(Long id) {
        return pizzaRepository.findById(id);
    }

    public Pizza create(Pizza pizza){
        return pizzaRepository.save(pizza);
    }

    public Boolean delete(Long id){

        Boolean wasDeleted = Boolean.FALSE;
        Optional<Pizza> pizza = getPizzaById(id);

        if (pizza.isPresent()){
            pizzaRepository.delete(pizza.get());
            wasDeleted = Boolean.TRUE;
        }

        return wasDeleted;

    }


    public Boolean updateCustomer(Long id, Pizza pizza){

        Boolean wasUpdated = Boolean.FALSE;

        Optional<Pizza> updatedPizza = getPizzaById(id);
        if(updatedPizza.isPresent()){
            updatedPizza.get().setName(pizza.getName());
            updatedPizza.get().setDescription(pizza.getDescription());
            updatedPizza.get().setPrice(pizza.getPrice());

            pizzaRepository.save(updatedPizza.get());
            wasUpdated = Boolean.TRUE;
        }

        return wasUpdated;
    }


    public Boolean update(Long id,Pizza newPizza){
        Boolean wasUpdated = Boolean.FALSE;
        Optional<Pizza> oldPizza = getPizzaById(id);

        if (oldPizza.isPresent()){
            oldPizza.get().setName(newPizza.getName());
            oldPizza.get().setDescription(newPizza.getDescription());
            oldPizza.get().setPrice(newPizza.getPrice());
            pizzaRepository.save(oldPizza.get());
            wasUpdated = Boolean.TRUE;
        }

        return wasUpdated;
    }

    public List<Pizza> getPizzaByName(String name){
        return pizzaRepository.findByName(name);
    }

}
