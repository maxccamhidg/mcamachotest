package com.example.demo.web;

import com.example.demo.domain.model.Customer;
import com.example.demo.domain.model.Order;
import com.example.demo.resources.CustomerResource;
import com.example.demo.service.CustomerService;
//import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping("/customers")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @RequestMapping(
            method = RequestMethod.GET
    )
    public List<CustomerResource> findAllCustomers(){

        List<CustomerResource> customerResources = new ArrayList<>();

        customerService.getAllCustomers().forEach(
                customer -> customerResources.add( new CustomerResource(customer))
        );

        return customerResources;

        //return new ResponseEntity<Iterable<Customer>>(customerService.getAllCustomers(), HttpStatus.OK);
    }

    @RequestMapping(
            method = RequestMethod.POST
    )
    private ResponseEntity<Customer> createCustomer(@RequestBody Customer customer){
        return new ResponseEntity<Customer>(customerService.createCustomer(customer), HttpStatus.CREATED);
    }

    @RequestMapping(
            method = RequestMethod.GET,
            value = "/{id}"
    )
    public ResponseEntity<Customer> findCustomerById(@PathVariable Long id){

        Optional<Customer> customer = customerService.getCustomerById(id);

        if(customer.isPresent()){
            return new ResponseEntity<Customer>(customer.get(), HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    @RequestMapping(
            value = "/{id}",
            method = RequestMethod.DELETE
    )
    public ResponseEntity<Void> deleteCustomer(@PathVariable Long id) {

        Boolean wasDeleted = customerService.deleteCustomer(id);

        HttpStatus responseStatus = wasDeleted ? HttpStatus.NO_CONTENT : HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(responseStatus);
    }


    @RequestMapping(
            value = "/{id}",
            method = RequestMethod.PUT
    )
    public ResponseEntity<Customer> updateCustomer(@PathVariable Long id, @RequestBody Customer updatedCustomer){

        Boolean wasUpdated =  customerService.updateCustomer(id, updatedCustomer);
        if(wasUpdated){
            return findCustomerById(id);
        }

        return new ResponseEntity<Customer>(HttpStatus.NOT_FOUND);
    }


    @RequestMapping(
            method = RequestMethod.GET,
            value = "/{id}/orders"
    )
    public Set<Order> getOrdersById(@PathVariable Long id) {
        return  customerService.getOrdersById(id);
    }

}
