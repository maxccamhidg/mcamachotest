package com.example.demo.web;

import com.example.demo.domain.model.Pizza;
import com.example.demo.resources.PizzaResource;
import com.example.demo.service.PizzaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Api(value = "online store", description = "Products online store")
@RestController
@RequestMapping("/products" )
public class PizzaController {

    @Autowired
    private PizzaService pizzaService;

    @Autowired
    //private ProductResourceAssembler assembler;

    @RequestMapping(
            method = RequestMethod.GET
    )
    public ResponseEntity<Collection<PizzaResource>> findAll(@RequestParam(value = "name", required = false) String name){

//        if(name != null && !name.trim().isEmpty()){
//            return new ResponseEntity<Iterable<Product>>(productService.getProductsByName(name), HttpStatus.OK);
//        }

        List<Pizza> products = pizzaService.getAllPizzas();
        return new ResponseEntity<>(assembler.toResourceCollection(products), HttpStatus.OK);
    }

    @RequestMapping(
            method = RequestMethod.POST
    )
    private ResponseEntity<PizzaResource> create(@RequestBody Pizza pizza){

        Pizza createProduct = pizzaService.create(pizza);

        return new ResponseEntity<>(assembler.toResource(createProduct), HttpStatus.CREATED);
    }

    @RequestMapping(
            method = RequestMethod.GET,
            value = "/{id}"
    )
    public ResponseEntity<PizzaResource> findProductById(@PathVariable Long id){

        Optional<Pizza> pizza = pizzaService.getPizzaById(id);

        if(pizza.isPresent()){
            return new ResponseEntity<>(assembler.toResource(pizza.get()), HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(
            method = RequestMethod.DELETE,
            value = "/{id}"
    )
    public ResponseEntity<Void> deleteProductById(@PathVariable Long id){

        Boolean wasDeleted = pizzaService.delete(id);

        HttpStatus responseStatus = wasDeleted ? HttpStatus.NO_CONTENT : HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(responseStatus);
    }

    @RequestMapping(
            value="/{id}",
            method = RequestMethod.PUT
    )
    public ResponseEntity<PizzaResource> updateProduct(@PathVariable Long id, @RequestBody Pizza pizza){

        Boolean wasUpdated = pizzaService.update(id, pizza);

        if(wasUpdated){
            return findProductById(id);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
